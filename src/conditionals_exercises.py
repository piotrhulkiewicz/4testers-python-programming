def display_speed_information(speed):
    if speed > 50:
        print("Slow down")
    else:
        print("Your speed is ok")


def calculate_fine_amount(speed):
    return 500 + (speed - 50) * 10


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print("Your speed was: ", speed)
    if speed > 100:
        print(f"You just lost your driving licence. Fine amount is {calculate_fine_amount(speed)}")
    elif speed > 50:
        print(f"You just got a fine. Fine amount is {calculate_fine_amount(speed)}")
    else:
        print("Thank You. Your speed is fine")


def check_normal_weather_conditions(temperature_in_celsius, pressure_in_hectopacals):
    if temperature_in_celsius == 0 and pressure_in_hectopacals == 1013:
        return True
    else:
        return False


def get_grades_info(grade):
    if not isinstance(grade, int) and not isinstance(grade, float):
        return "wpisz liczbę"
    if grade < 2 or grade > 5:
        return "N/A"
    if grade > 4.5:
        return "bardzo dobry"
    elif grade > 4.0:
        return "dobry"
    elif grade > 3.0:
        return "dostateczny"
    else:
        return "niedostateczny"


if __name__ == '__main__':
    # zad 1.
    display_speed_information(49)
    display_speed_information(50)
    display_speed_information(51)

    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(57)
    print_the_value_of_speeding_fine_in_built_up_area(80)

    # zad 2.
    print(check_normal_weather_conditions(0, 1013))
    print(check_normal_weather_conditions(1, 1013))
    print(check_normal_weather_conditions(0, 1014))
    print(check_normal_weather_conditions(1, 1014))

    # zad 3.
    print(get_grades_info(5))
    print(get_grades_info(4))
    print(get_grades_info(3))
    print(get_grades_info(2))
    print(get_grades_info(1))
    print(get_grades_info("string"))
    print(get_grades_info(6))
