import random
import string


def calculate_average_of_numbers_in_the_list(list_of_numbers):
    return sum(list_of_numbers) / len(list_of_numbers)


def calculate_average_of_two_numbers(number1, number2):
    return (number1 + number2) / 2


def generate_random_string(lenght):
    letters = string.ascii_letters + string.digits + string.punctuation
    return ''.join(random.choice(letters) for _ in range(lenght))


def generate_dictionary_using_email_and_random_password(user_email):
    return {
        "email": user_email,
        "password": generate_random_string(15)
    }


def calculate_player_level(exp_points):
    return exp_points // 1000
    # return int(exp_points / 1000)


def print_player_description(player_data):
    player_level = calculate_player_level(player_data['exp_points'])
    print(f"The player '{player_data['nick']}' is of type {player_data['type'].capitalize()} "
          f"and has {player_data['exp_points']} EXP, which is Level {player_level}")


if __name__ == '__main__':
    # exercise 1
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]

    average_temperature_in_january = calculate_average_of_numbers_in_the_list(january)
    average_temperature_in_february = calculate_average_of_numbers_in_the_list(february)

    print(f"Average temperature in January and february is "
          f"{calculate_average_of_two_numbers(average_temperature_in_january, average_temperature_in_february)}")

    # exercise 2
    email = "user@examle.com"
    credentials = generate_dictionary_using_email_and_random_password(email)
    print(credentials)

    # exercise 3
    player1_data = {
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    }

    print_player_description(player1_data)
