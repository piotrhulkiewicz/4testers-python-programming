shopping_list = ['oranges', 'water', 'chicken', 'potatoes', 'washing liquid']
print(shopping_list[0])
print(shopping_list[2])
print(shopping_list[-1])
print(shopping_list[-2])

shopping_list.append('lemons')
print(shopping_list)
print(shopping_list[-1])
shopping_list_lenght = len(shopping_list)
print(shopping_list_lenght)

first_three_shopping_items = shopping_list[0:3]
print(first_three_shopping_items)

# dictionaries
animal = {
    "name": "Burek",
    "kind": "dog",
    "age": 7,
    "male": True
}
dog_age = animal["age"]
print(f"Dog age: {dog_age}")
print(animal["name"])

animal["name"] = "Ciapek"
print(animal)

animal["owner"] = "Staszek"
print(animal)

