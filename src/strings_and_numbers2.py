first_name = "Piotr"
last_name = "Hulkiewicz"
email = "piotr.hulkiewicz@interia.pl"

print("My name is ", first_name, ". ", "My last name is ", last_name, ". My email: ", email, sep="")

# concatenation

my_bio = "My name is " + first_name + ". My last name is " + last_name + ". My email: " + email
print(my_bio)

# F-string
my_bio_using_f_string = f"My name is {first_name}.\nMy last name is {last_name}.\nMy email: {email}"
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4 przez 5 wynosi {4 * 5}")

# algebra

circle_radius = 7
area_of_circle = 3.14 * circle_radius ** 2
circumference_of_circle = 2 * 3.14 * circle_radius

print(
    f"Area of circle with radius {circle_radius} is {area_of_circle}.\nCircumference of circle with radius {circle_radius} is {circumference_of_circle}")

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7 ** 2 - 13

print(long_mathematical_expression)
