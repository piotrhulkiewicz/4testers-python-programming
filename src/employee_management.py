import random
# import time
import uuid


def create_random_mail(list_of_names):
    # return f"{random.choice(list_of_names)}.{random.randint(1, 1_000_000)}@example.com" generowanie z wykorzystaniem int
    # return f"{random.choice(list_of_names)}.{time.time()}@example.com" generowanie z wykorzystaniem unixtime
    return f"{random.choice(list_of_names)}.{str(uuid.uuid4())}@example.com"  # generowanie z wykorzystaniem uuid


def generate_seniority_years():
    return random.randint(0, 40)


def create_random_bool():
    return random.choice([True, False])
    # return bool(random.randint(0, 1)) rzutowanie na bool


def create_random_employee_dictionary():
    return {
        "email": create_random_mail(list_of_names),
        "seniority_level": generate_seniority_years(),
        "female": create_random_bool()
    }


def create_list_of_random_employee_dictionaries(number_of_dictionary):
    list_of_dictionaries = []
    for i in range(number_of_dictionary):
        list_of_dictionaries.append(create_random_employee_dictionary())
    return list_of_dictionaries


def get_emails_of_employees_with_seniority_over_10_years(list_of_employees, minimum_seniority_years):
    list_of_emails = []
    for employee in list_of_employees:
        if employee["seniority_level"] > minimum_seniority_years:
            list_of_emails.append(employee["email"])
    return list_of_emails


def get_female_dictionaries_from_list(list_of_employees, is_female):
    list_of_female_dictionaries = []
    for employee in list_of_employees:
        if employee["female"] == is_female:
            list_of_female_dictionaries.append(employee)
    return list_of_female_dictionaries


if __name__ == '__main__':
    list_of_names = ["John", "Anna", "Kate", "Adam", "Mark"]
    # zad 1
    print(create_random_employee_dictionary())
    print(create_random_employee_dictionary())
    print(create_random_employee_dictionary())
    print(create_random_employee_dictionary())
    # zad 2
    list_of_employees = create_list_of_random_employee_dictionaries(5)
    print(list_of_employees)
    # zad 3
    print(get_emails_of_employees_with_seniority_over_10_years(list_of_employees, 10))
    print(get_female_dictionaries_from_list(list_of_employees, True))
