def print_a_car_brand_name():
    print("Honda")


def print_given_number_multiplied_by_3(input_number):
    print(input_number * 3)


def calculate_area_of_a_circle(radius_in_centimetres):
    return 3.1415 * radius_in_centimetres ** 2


def calculate_area_of_triangle(height, bottom):
    return 0.5 * bottom * height


if __name__ == '__main__':
    print_a_car_brand_name()
    print_given_number_multiplied_by_3(10)
    area_of_a_circle = calculate_area_of_a_circle(10)
    print(area_of_a_circle)
    print(calculate_area_of_triangle(5, 5))
