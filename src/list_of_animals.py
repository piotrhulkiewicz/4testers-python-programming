animals = [
    {"name": "Burek", "kind": "dog", "age": 7},
    {"name": "Bonifacy", "kind": "cat", "age": None},
    {"name": "Misio", "kind": "hamster", "age": 1}

]
print(animals[-1]["name"])
animals[1]["age"] = 2
print(animals[1])
animals.insert(0, {"name": "Reksio", "kind": "dog", "age": 7})
animals.append({"name": "Mila", "kind": "cat", "age": 4})
print(animals)
