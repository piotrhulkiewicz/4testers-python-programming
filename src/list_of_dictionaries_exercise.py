addresses = [
    {"city": "Opole", "street": "Wrocławska", "house_number": "8", "post_code": "45-753"},
    {"city": "Wrocław", "street": "Górna", "house_number": "123", "post_code": "54-736"},
    {"city": "Poznań", "street": "Podmiejska", "house_number": "45a", "post_code": "34-758"},
]


if __name__ == '__main__':

    print(addresses[-1]["post_code"])
    print(addresses[1]["city"])
    addresses[0]["street"] = "Kwiatowa"
    print(addresses)
