animal1 = {
    "kind": "dog",
    "age": 2,
    "male": True
}
animal2 = {
    "kind": "cat",
    "age": 5,
    "male": False
}
animal3 = {
    "kind": "fish",
    "age": 1,
    "male": True
}

animals = [animal1, animal2, animal3]

print(len(animals))
print(animals[0])
print(f"The age of the last animal is equal to {animals[-1]['age']}")
animals.insert(2,
               {
                   "kind": "zebra",
                   "age": 7,
                   "male": False
               }
               )

animals.append(
    {
        "kind": "camel",
        "age": 11,
        "male": True
    }
)
print(animals)
