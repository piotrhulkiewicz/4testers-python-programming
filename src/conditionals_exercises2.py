def display_speed_information(speed):
    if speed <= 50:
        print('Thank you, your speed is ok')
    else:
        print('Slow down')


def calculate_the_value_of_fine(speed):
    return (speed - 50) * 10 + 500


def print_the_value_of_speeding_fine_in_built_up_area(speed):
    print("Your speed was: ", speed)
    if speed > 100:
        print(f"Fine amount: {calculate_the_value_of_fine(speed)} PLN. You have just lost your driving licence")
    elif speed > 50:
        print(f"You got a fine!. Fine amount: {calculate_the_value_of_fine(speed)} PLN.")
    else:
        print("Thank you")


def check_if_weather_conditions_are_ok(temperature_in_celsius, pressure_in_hectopascals):
    return True if temperature_in_celsius == 0 and pressure_in_hectopascals == 1013 else False


def get_grade_info(grade):
    if not (isinstance(grade, int) or type == float):
        return "N/A"
    elif grade > 5.0 or grade < 2.0:
        return "N/A"
    elif grade >= 4.5:
        return "Bardzo dobry"
    elif grade >= 4.0:
        return "Dobry"
    elif grade >= 3.0:
        return "Dostateczny"
    else:
        return "Niedostateczny"


if __name__ == '__main__':
    # exercise 1
    display_speed_information(49)
    display_speed_information(50)
    display_speed_information(51)

    print_the_value_of_speeding_fine_in_built_up_area(101)
    print_the_value_of_speeding_fine_in_built_up_area(100)
    print_the_value_of_speeding_fine_in_built_up_area(99)
    print_the_value_of_speeding_fine_in_built_up_area(51)
    print_the_value_of_speeding_fine_in_built_up_area(50)
    print_the_value_of_speeding_fine_in_built_up_area(49)
    print_the_value_of_speeding_fine_in_built_up_area(60)

    # exercise 2
    print(check_if_weather_conditions_are_ok(0, 1013))
    print(check_if_weather_conditions_are_ok(1, 1013))
    print(check_if_weather_conditions_are_ok(0, 1014))
    print(check_if_weather_conditions_are_ok(1, 1014))

    # exercise 3
    print(get_grade_info(5))
    print(get_grade_info(4.5))
    print(get_grade_info(4))
    print(get_grade_info(3))
    print(get_grade_info(2))
    print(get_grade_info(1))
    print(get_grade_info(6))
    print(get_grade_info('A'))
