def print_greetings_for_a_person_in_the_city(city, first_name):
    print(f"Witaj {first_name}! Miło Cię widzieć w naszym mieście: {city}")


def generate_email(first_name, last_name):
    print(f"{first_name.lower()}.{last_name.lower()}@testers.pl")


if __name__ == '__main__':
    print_greetings_for_a_person_in_the_city("Opole", "Piotr")
    print_greetings_for_a_person_in_the_city("Wrocław", "Paweł")
    generate_email("Janusz", "Nowak")
    generate_email("Barbara", "Kowalska")
