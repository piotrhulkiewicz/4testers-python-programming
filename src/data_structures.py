movies = ['Star Wars', 'Diune', 'LOTR', "Blade Runner", "Stalker"]
last_movie = movies[-1]
movies.append("Lost Highway")
movies.append("Titanic")
print(len(movies))
middle_movies_range = movies[2:6]
print(middle_movies_range)
movies.insert(0, "Top Gun")
print(movies)

emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@exapmple.com')
print(emails)

friend = {
    "first_name": "Tomasz",
    "age": 43,
    "hobby": ["sport", "motoryzacja"]
}
friend_hobbies = friend["hobby"]
print(f"My friend has {len(friend_hobbies)} hobbies")
print(friend)
print(friend_hobbies)

friend["hobby"].append("football")
print(friend)

friend["married"] = True
print(friend)


