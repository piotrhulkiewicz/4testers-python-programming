movies = ['Diune', 'Stalker', 'Star Wars', 'Lost Highway', 'Blade Runner']
last_movie = movies[-1]
movies.append('Lord of the rings')
movies.append('Titanic')
print(len(movies))

middle_movies = movies[2:5]
print(middle_movies)

movies.insert(1, 'Top Gun')
print(movies)

emails = ['a@example.com', 'b@example.com']

print(len(emails))
print(emails[0])
print(emails[-1])
emails.append('cde@example.com')
print(emails)

friend = {
    "first_name": "Tomasz",
    "age": 44,
    "hobbies": ["sport", "motorization"]
}

friend_hobbies = friend["hobbies"]
print(f"Hobbies of my friend: {friend_hobbies}")
print(f"My friend has {len(friend["hobbies"])} hobbies")

friend["hobbies"].append("football")
print(friend["hobbies"])

friend["married"] = True
print(friend)
