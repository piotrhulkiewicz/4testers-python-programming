class Cup:
    def __init__(self, volume):
        self.volume = volume
        self.current_liquid_volume = 0

    def fill_the_cup(self, amount_of_liquid):
        free_space = self.volume - self.current_liquid_volume
        if amount_of_liquid > free_space:
            self.current_liquid_volume = self.volume
        else:
            self.current_liquid_volume += amount_of_liquid

    def drink(self, amount_of_liquid):
        if amount_of_liquid > self.current_liquid_volume:
            self.current_liquid_volume = 0
        else:
            self.current_liquid_volume -= amount_of_liquid


if __name__ == '__main__':
    cup1 = Cup(500)
    cup1.fill_the_cup(500)
    cup1.drink(50)
    print(cup1.current_liquid_volume)
