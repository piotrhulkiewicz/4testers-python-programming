class Cup:
    def __init__(self, max_volume):
        self.max_volume = max_volume
        self.current_volume = 0

    def fill_the_cup(self, amount_of_liquid):
        if amount_of_liquid <= self.max_volume - self.current_volume:
            self.current_volume += amount_of_liquid
        else:
            self.current_volume = self.max_volume

    def drink(self, amount_of_liquid):
        if amount_of_liquid <= self.current_volume:
            self.current_volume -= amount_of_liquid
        else:
            self.current_volume = 0


if __name__ == '__main__':
    cup1 = Cup(500)
    cup1.fill_the_cup(500)
    cup1.drink(50)
    print(cup1.current_volume)
