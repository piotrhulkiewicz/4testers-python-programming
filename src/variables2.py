first_name = "Piotr"
last_name = "Hulkiewicz"
age = 44

print(first_name)
print(last_name)
print(age)

# description of my dog
name = "Panela"
coat_color = 'Black/brown'
age_in_months = 5
number_of_paws = 4
weight_in_kilograms = 5.1
is_male = False
has_chip = True
is_vaccinated = True

print(name, age_in_months, weight_in_kilograms, number_of_paws, coat_color, is_male)

# my best friend description
best_friend_name = "Tomek"
age_in_years = 44
amount_of_animals = 0
has_driving_licence = True
friendship_duration_in_years = 35.5

print(best_friend_name, age_in_years, amount_of_animals, has_driving_licence, friendship_duration_in_years, sep=",")
