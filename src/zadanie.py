import random


def generate_random_firstname(fnames):
    return random.choice(fnames)


def generate_random_lastname(surnames):
    return random.choice(surnames)


def generate_random_age():
    return random.randint(5, 45)


def generate_random_country(countries):
    return random.choice(countries)


def is_adult(age):
    return True if age >= 18 else False


def caclulate_birth_year(age):
    return 2022 - age


def generate_random_person_describe(fnames, surnames, countries):
    firstname = generate_random_firstname(fnames)
    lastname = generate_random_lastname(surnames)
    email = f"{firstname.lower()}.{lastname.lower()}@example.com"
    age = generate_random_age()
    country = generate_random_country(countries)
    adult = is_adult(age)
    birth_year = caclulate_birth_year(age)
    return {
        'firstname': firstname,
        'lastname': lastname,
        'email': email,
        'age': age,
        'country': country,
        'adult': adult,
        'birth_year': birth_year
    }


def generate_random_female_describe():
    return generate_random_person_describe(female_fnames, surnames, countries)


def generate_random_male_describe():
    return generate_random_person_describe(male_fnames, surnames, countries)


def generate_5_male_and_5_female_random_person_data():
    list_of_random_person = []
    for i in range(1, 5):
        list_of_random_person.append(generate_random_female_describe())
    for i in range(1, 5):
        list_of_random_person.append(generate_random_male_describe())
    return list_of_random_person


def print_info_about_people_on_the_list(list_of_people):
    for person in list_of_people:
        print(
            f"Hi! I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}")


if __name__ == '__main__':
    # Listy zawierające dane do losowania
    female_fnames = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
    male_fnames = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
    surnames = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
    countries = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']

    list_of_5_random_male_and_female_data = generate_5_male_and_5_female_random_person_data()
    print_info_about_people_on_the_list(list_of_5_random_male_and_female_data)
