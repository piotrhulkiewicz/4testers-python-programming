def calculate_the_square_of_the_number(input_number):
    return input_number ** 2


def convert_temperature_from_celsius_to_fahrenheit(temp_in_celsius):
    return temp_in_celsius * 1.8 + 32


def calculate_the_volume_of_a_cuboid(height, width, lenght):
    return height * width * lenght


if __name__ == '__main__':
    # exercise 1
    print(f"square of 0: {calculate_the_square_of_the_number(0)}\n"
          f"square of 16: {calculate_the_square_of_the_number(16)}\n"
          f"square of 2.55: {calculate_the_square_of_the_number(2.55)}")

    # exercise 2
    print(f"20 degrees in celsius is {convert_temperature_from_celsius_to_fahrenheit(20)} in fahreinheit")

    # exercise 3
    print(f"The volume of the cuboid with dimensions 3,5,7 is:"
          f" {calculate_the_volume_of_a_cuboid(5, 5, 5)}")
