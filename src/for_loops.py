def print_each_student_name_capitalized(list_of_student_first_names):
    for name in list_of_student_first_names:
        print(name.capitalize())


def print_x_integer_numbers_squared(x):
    for number in range(1, x):
        print(number ** 2)


if __name__ == '__main__':
    list_of_students = ["kate", "mark", "stephane", "nicki", "anna"]
    print_each_student_name_capitalized(list_of_students)

print_x_integer_numbers_squared(11)
