first_name = "Piotr"
last_name = "Hulkiewicz"
email = "piotr.hulkiewicz@interia.pl"

print("My first name is ", first_name, ".", "My last name is ", last_name, ". ", sep="")
my_bio = "My first name is " + first_name + ". My last name is " + last_name + ". My emai is " + email
print(my_bio)

# F-string

my_bio_using_f_string = f"My first name is {first_name}.\nMy last name is {last_name}.\nMy email is {email}"
print(my_bio_using_f_string)

print(f"Wynik operacji mnożenia 4*5 to: {4 * 5}")

# Algebra

circle_radius = 10
area_of_circle_with_radius = 3.14 * circle_radius ** 2
circumference_of_circle_with_radius = 2 * 3.14 * circle_radius

print(f"Area of circle with radius {circle_radius} is {area_of_circle_with_radius}")
print(f"circumference of circle with radius {circle_radius} is {circumference_of_circle_with_radius}")

long_mathematical_expression = 2 + 3 + 5 * 7 + 4 / 3 * (3 + 5 / 2) + 7 ** 2 - 13

print(long_mathematical_expression)
