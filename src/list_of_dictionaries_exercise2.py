addresses = [
    {'city': 'Opole', 'street': 'Ozimska', 'house_number': '142', 'post_code': '45-960'},
    {'city': 'Brzeg', 'street': 'Opolska', 'house_number': '64A', 'post_code': '46-370'},
    {'city': 'Wrocław', 'street': 'Ślężna', 'house_number': '123B', 'post_code': '48-211'}
]


if __name__ == '__main__':
    print(addresses[-1]['post_code'])
    print(addresses[1]['city'])
    addresses[0]['street'] = 'Górna'
    print(addresses)
