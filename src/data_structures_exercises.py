import string
import random


def calculate_sum_of_numbers_in_the_list(input_list):
    return sum(input_list)


def calculate_average_of_two_numbers(a, b):
    return (a + b) / 2


def generate_random_password():
    n = 30
    random_password = ''.join(random.choices(string.ascii_lowercase + string.digits + string.punctuation, k=n))
    return random_password


def calculate_palyer_level(experience_ponts):
    return experience_ponts // 1000
    # return int(experience_ponts / 1000)


def generate_random_login_data(user_email):
    return {
        "email": user_email,
        "password": generate_random_password()
    }


def get_players_dictionary_and_print_description(player_dictionary):
    level = calculate_palyer_level(player_dictionary['exp_points'])
    print(
        f"The player {player_dictionary['nick']} is of type {player_dictionary['type'].capitalize()} and has {player_dictionary['exp_points']} EXP. The player is on level {level}")


if __name__ == '__main__':
    #  zad 1.
    january = [-4, 1.0, -7, 2]
    february = [-13, -9, -3, 3]
    average_temperature_in_january = calculate_sum_of_numbers_in_the_list(january) / len(january)
    average_temperature_in_february = calculate_sum_of_numbers_in_the_list(february) / len(february)
    print(
        f"Average temerature in january and february is {calculate_average_of_two_numbers(average_temperature_in_january, average_temperature_in_february)}")

    # zad 2.
    print(generate_random_login_data("piotr@example.com"))
    print(generate_random_login_data("marek@example.com"))
    print(generate_random_login_data("kasia@example.com"))

    # zad 3.
    get_players_dictionary_and_print_description({
        "nick": "maestro_54",
        "type": "warrior",
        "exp_points": 3000
    })
