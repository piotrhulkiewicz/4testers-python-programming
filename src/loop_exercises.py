from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print("Hello!", i)


def print_positive_numbers_divisible_by_7(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_x_random_numbers(x):
    for i in range(x):
        print(f"loop {i + 1}: ", randint(1, 100))


def get_square_roots_of_numbers(list_of_numbers):
    new_list = []
    for number in list_of_numbers:
        sqrt_of_number = round(sqrt(number), 2)
        new_list.append(sqrt_of_number)
    return new_list


def transform_list_of_temperatures_from_celsius_to_fahrenheit(list_of_temperatures_in_celsius):
    temperatures_in_fahrenheit = []
    for temperature in list_of_temperatures_in_celsius:
        temperatures_in_fahrenheit.append(temperature * 1.8 + 32)
    return temperatures_in_fahrenheit


if __name__ == '__main__':
    # zad 1
    print_hello_40_times()

    # zad 2
    print_positive_numbers_divisible_by_7(1, 100)

    # zad 3
    print_x_random_numbers(5)

    # zad 4
    numbers = [1, 7, 642, 162, 25]
    print(get_square_roots_of_numbers(numbers))

    # zad 5
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    temps_in_fahrenheit = transform_list_of_temperatures_from_celsius_to_fahrenheit(temps_celsius)
    print(temps_in_fahrenheit)
