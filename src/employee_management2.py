import random
import uuid


def generate_random_email():
    domain = 'example.com'
    list_of_names = ['ania', 'basia', 'kasia', 'veronica']
    random_name = random.choice(list_of_names)
    suffix = str(uuid.uuid4())
    return f"{random_name}.{suffix}@{domain}"


def generate_random_seniority():
    return random.randint(0, 40)


def generate_random_bool():
    # random_bool = random.randint(1, 3)
    # return True if random_bool == 1 else False
    return random.choice([True, False])


def generate_random_employee_data():
    random_email = generate_random_email()
    random_seniority = generate_random_seniority()
    random_bool = generate_random_bool()
    return {
        'email': random_email,
        'seniority_years': random_seniority,
        'female': random_bool
    }


def generate_list_of_employees_dictionaries(n):
    list_of_dictionaries = []
    for i in range(1, n + 1):
        list_of_dictionaries.append(generate_random_employee_data())
    return list_of_dictionaries


def get_list_of_emails_of_employees_with_n_seniority_above(list_of_employees, n):
    list_of_emails_sen_above_10 = []
    for employee in list_of_employees:
        if employee['seniority_years'] > n:
            list_of_emails_sen_above_10.append(employee['email'])
    return list_of_emails_sen_above_10


def get_employees_filtered_by_gender(list_of_employees, is_female):
    list_of_filtered_employees = []
    for employee in list_of_employees:
        if employee['female'] == is_female:
            list_of_filtered_employees.append(employee)
    return list_of_filtered_employees


if __name__ == '__main__':
    # # exercise 1
    # print(generate_random_employee_data())
    # # exercise 2
    # print(generate_list_of_employees_dictionaries(10))
    # # exercise 3
    list_of_employees_data = generate_list_of_employees_dictionaries(10)
    print(list_of_employees_data)
    list_of_emails = get_list_of_emails_of_employees_with_n_seniority_above(list_of_employees_data, 20)
    print(list_of_emails)
    list_of_male_employees = get_employees_filtered_by_gender(list_of_employees_data, False)
    print(list_of_male_employees)
