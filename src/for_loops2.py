def print_each_student_name_capitalized(list_of_first_names):
    for name in list_of_first_names:
        print(name.capitalize())


def print_first_ten_integers_squared():
    for integer in range(1, 10):
        print(integer * integer)


if __name__ == '__main__':
    list_of_students = ["kate", "mark", "ADAM", "toM", "mARTHa"]
    print_each_student_name_capitalized(list_of_students)
    print_first_ten_integers_squared()
