import random
from datetime import datetime

female_fnames_list = ['Kate', 'Agnieszka', 'Anna', 'Maria', 'Joss', 'Eryka']
male_fnames_list = ['James', 'Bob', 'Jan', 'Hans', 'Orestes', 'Saturnin']
surnames_list = ['Smith', 'Kowalski', 'Yu', 'Bona', 'Muster', 'Skinner', 'Cox', 'Brick', 'Malina']
countries_list = ['Poland', 'United Kingdom', 'Germany', 'France', 'Other']
min_age = 5
max_age = 45


def generate_random_firstname(fnames):
    return random.choice(fnames)


def generate_random_lastname(surnames):
    return random.choice(surnames)


def generate_random_country(countries):
    return random.choice(countries)


def generate_random_age():
    return random.randint(min_age, max_age)


def get_is_adult(age):
    return True if age >= 18 else False


def get_year_of_birth(age):
    return datetime.now().year - age


def generate_person_data(fnames, surnames, countries):
    firstname = generate_random_firstname(fnames)
    lastname = generate_random_lastname(surnames)
    country = generate_random_country(countries)
    email = f'{firstname.lower()}.{lastname.lower()}@example.com'
    age = generate_random_age()
    return {
        'firstname': firstname,
        'lastname': lastname,
        'country': country,
        'email': email,
        'age': age,
        'adult': get_is_adult(age),
        'birth_year': get_year_of_birth(age)
    }


def generate_list_of_5_male_and_5_female_persons(female_fnames, male_fnames, surnames, countries, female_counter,
                                                 male_counter):
    list_of_persons_data = []
    for person in range(1, female_counter + 1):
        list_of_persons_data.append(generate_person_data(female_fnames, surnames, countries))
    for person in range(1, male_counter + 1):
        list_of_persons_data.append(generate_person_data(male_fnames, surnames, countries))
    return list_of_persons_data


def print_list_of_persons_info(random_persons_list):
    for person in random_persons_list:
        print(
            f"Hi. I'm {person['firstname']} {person['lastname']}. I come from {person['country']} and I was born in {person['birth_year']}")


if __name__ == '__main__':
    persons_list = generate_list_of_5_male_and_5_female_persons(female_fnames_list, male_fnames_list, surnames_list,
                                                                countries_list, 5, 5)
    print(persons_list)
    print_list_of_persons_info(persons_list)
