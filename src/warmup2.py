def convert_celsius_to_fahrenheit(temp_in_Celsius):
    return temp_in_Celsius * 1.8 + 32


def calculate_2nd_power_of_number(input_number):
    return input_number ** 2


def calculate_volume_of_cuboid(height, width, lenght):
    return height * width * lenght


if __name__ == '__main__':
    print(f"O do kwadratu to: {calculate_2nd_power_of_number(0)}\n"
          f"16 do kwadratu to: {calculate_2nd_power_of_number(16)}\n"
          f"2.55 do kwadratu to {calculate_2nd_power_of_number(2.55)}")
    print(f"20 stopni Celsjusza to {convert_celsius_to_fahrenheit(20)} stopni Fafrenheita")
    print(f"Objętość prostopadłościanu o wymiarach 3, 5, 7, to {calculate_volume_of_cuboid(3, 5, 7)}")
