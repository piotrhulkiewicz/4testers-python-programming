animals = [
    {
        "kind": "dog",
        "age": 5,
        "male": True
    },
    {
        "kind": "cat",
        "age": 3,
        "male": False
    },
    {
        "kind": "fish",
        "age": 1,
        "male": True
    }
]

print(len(animals))
print(animals[0]['age'])

animals.append({
    "kind": "zebra",
    "age": 7,
    "male": False
})
print(animals)
animals.insert(0, {
    "kind": "lion",
    "age": 10,
    "male": False
})
print(animals)
