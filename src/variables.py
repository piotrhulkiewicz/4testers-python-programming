first_name = "Piotr"
last_name = "Hulkiewicz"
age = 43

print(first_name)
print(last_name)
print(age)

# my_dog_description
name = "Manela"
coat_color = "Black/brown"
age_in_months = 5
number_of_paws = 4
weight = 5.1
is_male = False
has_chip = True

print(name, age_in_months, weight, number_of_paws, coat_color, is_male)

friends_first_name = "Tomasz"
friends_age = 43
number_of_friends_pets = 0
has_driving_licence = True
friendship_duration_in_years = 35

print(friends_first_name, friends_age, number_of_friends_pets, has_driving_licence, friendship_duration_in_years, sep=", ")
