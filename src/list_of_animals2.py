animals = [
    {'name': 'Burek', 'kind': 'dog', 'age': 7},
    {'name': 'Boniacy', 'kind': 'cat', 'age': None},
    {'name': 'Max', 'kind': 'hamster', 'age': 1},
]

if __name__ == '__main__':
    print(animals[-1]['name'])
    animals[1]['age'] = 2
    print(animals[1])
    animals.append({'name': 'Rex', 'kind': 'dog', 'age': 9})
    print(animals)
    animals.insert(0, {'name': 'Klaus', 'kind': 'fish', 'age': 2})
    print(animals[0])
