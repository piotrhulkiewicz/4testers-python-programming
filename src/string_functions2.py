def print_greetings_for_a_person_in_the_city(person_name, city):
    print(f"Hello {person_name}! Nice to see you in {city}.")


def generate_email(first_name, last_name):
    print(f"{first_name.lower()}.{last_name.lower()}@4testers.pl")


if __name__ == '__main__':
    # exercise 1
    print_greetings_for_a_person_in_the_city("Adam", "Opole")
    print_greetings_for_a_person_in_the_city("Kasia", "Poznań")
    # exercise 2
    generate_email("Janusz", "Biały")
    generate_email("Barbara", "Kowalska")
