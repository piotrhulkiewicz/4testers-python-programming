def print_weather_description(temperature_in_celsius):
    print(f"Temperature right now is {temperature_in_celsius} Celsius degrees")
    if temperature_in_celsius > 25:
        print("It's getting hot!")
    elif temperature_in_celsius < 10:
        print("It's getting cold!")
    else:
        print("It's quite OK.")


def is_person_an_adult(age):
    if age >= 18:
        return True
    else:
        return False
    # return True if age >= 18 else False


if __name__ == '__main__':
    temperature_in_celsius = 3
    print_weather_description(temperature_in_celsius)
    age_kate = 17
    age_tom = 18
    age_martha = 21
    print("Kate", is_person_an_adult(age_kate))
    print("Tom", is_person_an_adult(age_tom))
    print("Martha", is_person_an_adult(age_martha))
