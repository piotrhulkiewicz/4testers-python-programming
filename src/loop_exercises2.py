from random import randint
from math import sqrt


def print_hello_40_times():
    for i in range(1, 41):
        print("Hello", i)


def print_positive_numbers_divisible_by_seven(start, stop):
    for i in range(start, stop + 1):
        if i % 7 == 0:
            print(i)


def print_n_random_numbers(n):
    for i in range(1, n + 1):
        print(i, randint(1, 1000))


def get_square_roots_of_numbers(list_of_numbers):
    square_roots = []
    for number in list_of_numbers:
        (square_roots.append(round(sqrt(number), 2)))
    return square_roots


def map_list_of_temperatures_from_celsius_to_fahrenheit(list_of_temperatures_in_celsius):
    temperatures_in_fahrenheit = []
    for temperature in list_of_temperatures_in_celsius:
        temperatures_in_fahrenheit.append(temperature * 1.8 + 32)
    return temperatures_in_fahrenheit


if __name__ == '__main__':
    # exercise 1
    print_hello_40_times()

    # exercise 2
    print_positive_numbers_divisible_by_seven(1, 30)

    # exercise 3
    print_n_random_numbers(10)
    print(sqrt(9))

    # exercise 4
    list_of_numbers = [4, 9, 16, 251]
    list_of_square_roots = get_square_roots_of_numbers(list_of_numbers)
    print(list_of_square_roots)

    # exercise 5
    temps_celsius = [10.3, 23.4, 15.8, 19.0, 14.0, 23.0, 25.0]
    test_temps = [-32 * 5/9, 0]
    temps_fahrenheit = map_list_of_temperatures_from_celsius_to_fahrenheit(temps_celsius)
    print(temps_fahrenheit)
    print(map_list_of_temperatures_from_celsius_to_fahrenheit(test_temps))