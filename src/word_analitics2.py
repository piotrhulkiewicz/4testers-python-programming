def get_words_contains_a_from_the_list(word_list):
    list_of_words_with_a = []
    for word in word_list:
        if not isinstance(word, str):
            continue
        if 'a' in word:
            list_of_words_with_a.append(word)
    return list_of_words_with_a


if __name__ == '__main__':
    list_of_words = ['ala', 'kot']
    print(get_words_contains_a_from_the_list(list_of_words))
