def print_word_uppercase_and_capitalized(word):
    uppercase_word = word.upper()
    print(uppercase_word)
    capitalized_word = word.capitalize()
    print(capitalized_word)


if __name__ == '__main__':
    word = "kot"
    print_word_uppercase_and_capitalized(word)
