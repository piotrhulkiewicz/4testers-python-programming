from src.employee_management2 import generate_random_employee_data


def test_single_employee_generation_seniority_years_has_correct_value():
    test_seniority_years = generate_random_employee_data()
    employee_seniority_years = test_seniority_years['seniority_years']
    assert employee_seniority_years >= 0
    assert employee_seniority_years <= 40
    # assert type(employee_seniority_years) == int
    assert isinstance(employee_seniority_years, int)


def test_random_employee_generation_has_proper_keys():
    test_employee = generate_random_employee_data()
    # print(test_employee)
    assert 'female' in test_employee.keys()
    assert 'email' in test_employee.keys()
    assert 'seniority_years' in test_employee.keys()
