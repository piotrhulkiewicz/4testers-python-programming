from src.conditional_statements2 import is_person_an_adult


def test_age_over_18_years_should_be_an_adult():
    is_adult = is_person_an_adult(19)
    assert is_adult


def test_age_18_years_should_be_an_adult():
    is_adult = is_person_an_adult(18)
    assert is_adult


def test_age_under_18_years_should_not_be_an_adult():
    is_adult = is_person_an_adult(17)
    # assert not is_adult
    # assert is_adult == False
    assert is_adult != True
