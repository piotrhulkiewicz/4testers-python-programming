from src.oop.household_items2 import Cup


def test_fill_cup_over_the_max_volume():
    test_cup = Cup(500)
    test_cup.fill_the_cup(550)
    assert test_cup.current_volume == 500


def test_fill_cup_under_the_max_volume():
    test_cup = Cup(500)
    test_cup.fill_the_cup(400)
    assert test_cup.current_volume == 400


def test_fill_the_cup_twice():
    test_cup = Cup(500)
    test_cup.fill_the_cup(450)
    test_cup.fill_the_cup(100)
    assert test_cup.current_volume == 500


def test_drink_from_the_cup_more_than_the_current_volume():
    test_cup = Cup(500)
    test_cup.fill_the_cup(400)
    test_cup.drink(200)
    test_cup.drink(250)
    assert test_cup.current_volume == 0
