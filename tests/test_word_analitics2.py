from src.word_analitics2 import get_words_contains_a_from_the_list

test_case_list1 = []
test_case_list2 = [3, 5.19]
test_case_list3 = ['%', 'ogon', 'bar']
test_case_list4 = ['kot', 'mini', 'żubr']
test_case_list5 = ['akacja', 'akcja', 'licytacja', 'telefon']


def test_empty_list():
    test_list = get_words_contains_a_from_the_list(test_case_list1)
    assert test_list == []


def test_list_contains_numbers():
    test_list = get_words_contains_a_from_the_list(test_case_list2)
    assert test_list == []


def test_list_contains_special_characters():
    test_list = get_words_contains_a_from_the_list(test_case_list3)
    assert test_list == ['bar']


def test_list_with_no_word_contains_a():
    test_list = get_words_contains_a_from_the_list(test_case_list4)
    assert test_list == []


def test_list_with_words_contains_a():
    test_list = get_words_contains_a_from_the_list(test_case_list5)
    assert test_list == ['akacja', 'akcja', 'licytacja']
