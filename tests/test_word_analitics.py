from src.word_analitics import filter_words_containing_letter_a

list1 = []
list2 = [1, 2, 3]
list3 = ['$', '%']
list4 = ["pies", "kot", "rybik"]
list5 = ["laba", "breja"]


def test_filter_words_for_empty_list():
    filtered_word = filter_words_containing_letter_a(list1)
    assert filtered_word == []


def test_filter_int_list():
    filtered_word = filter_words_containing_letter_a(list2)
    assert filtered_word == []


def test_filtered_list_not_containing_any_word_with_a():
    filtered_word = filter_words_containing_letter_a(list4)
    assert filtered_word == []


def test_filtered_list_containing_words_wit_a():
    filtered_word = filter_words_containing_letter_a(list5)
    assert filtered_word == ["laba", "breja"]

